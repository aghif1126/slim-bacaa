<?php

namespace App\Controller;

use App\Model\AuthModel;
use App\Model\User;
use App\Model\Content;
use Pimple\Psr11\Container;
use App\Helper\JsonResponse;
use App\Repository\UploadFile;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AboutBioController
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->upload       = new UploadFile();
    }
    public function generateRandomString($length = 64) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function index(Request $request, Response $response): Response
    {
        $about = Content::where('type', 'About-Bio')->get();
        $result['status']   = true;
        $result['data']     = $about;
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function store(Request $request, Response $response): Response
    {
        $upload         = '';
        $req = $request->getParsedBody();
        if (isset($_FILES['image']) && $_FILES['image']['size'] != 0) {
                
            $targetFolder   = "";
            $validateFile   = $this->upload->validateFile('image', $targetFolder, true);

            if ($validateFile['status']) {
                $upload = $this->upload->moveUploadedOneS3('image', $this->generateRandomString(25).'.'.$validateFile['extension'], true);
            }
            
        }

        $payload = [
            'type'      => 'About-Bio',
            'attr'      => [
                'image' => $upload,
                'desc_en' => $req['desc_en'],
                'desc_in' => $req['desc_in'],
                'caption_en' => $req['caption_en'],
                'caption_in' => $req['caption_in'],
            ]
        ];

       
        if(Content::where('type', 'About-Bio')->first() != ''){

            $news = Content::where('type', 'About-Bio')->first();
            $news -> update($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil diubah';

        }else{      

            Content::create($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil ditambah';

        }
        return JsonResponse::withJson($response, $result, 200);
    }


    public function delete(Request $request, Response $response): Response
    {
        $params = $request->getQueryParams();
        $id     = isset($params['id']) ? $params['id'] : null;

        if($id){

            Content::find($id)->delete();
            $result['status']   = true;
            $result['message']   = 'data berhasil dihapus';

        }else{

            $result['status']   = false;
            $result['message']   = 'data tidak di temukan';
            
        }
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function uploadFile(Request $request, Response $response): Response
    {
        $upload         = '';

        if (isset($_FILES['image']) && $_FILES['image']['size'] != 0) {
            $targetFolder   = "";
            $validateFile   = $this->upload->validateFile('image', $targetFolder, true);

            if ($validateFile['status']) {
                $upload = $this->upload->moveUploadedOneS3('image', $this->auth->generateRandom(25).'.'.$validateFile['extension'], true);
            }
        }

        $result['status']    = true;
        $result['message']   = 'Successfully';
        $result['data']      = $upload;

        return JsonResponse::withJson($response, $result, 200);
    }
}
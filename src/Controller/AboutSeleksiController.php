<?php

namespace App\Controller;

use App\Model\AuthModel;
use App\Model\User;
use App\Model\Content;
use Pimple\Psr11\Container;
use App\Helper\JsonResponse;
use App\Repository\UploadFile;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class AboutSeleksiController
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        // $this->upload       = new UploadFile();
    }

    public function index(Request $request, Response $response): Response
    {
        $about = Content::where('type', 'About-Seleksi')->first();
        $result['status']   = true;
        $result['data']     = $about;
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function store(Request $request, Response $response): Response
    {

        $req = $request->getParsedBody();
    
        $payload = [
            'type'      => 'About-Seleksi',
            'attr'      => [
                'desc_en' => $req['desc_en'],
                'desc_in' => $req['desc_in'],
            ]
        ];

        $about = Content::where('type', 'About-Seleksi')->first();
        if($about != ''){

            $news = Content::where('type', 'About-Seleksi')->first();
            $news -> update($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil diubah';
     
            
        }else{      
            
            Content::create($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil ditambah';
      

        }
        return JsonResponse::withJson($response, $result, 200);
    }

}
<?php

namespace App\Controller;

use App\Model\AuthModel;
use App\Model\User;
use App\Model\Content;
use Pimple\Psr11\Container;
use App\Helper\JsonResponse;
use App\Repository\UploadFile;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class InfoRegistController
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        // $this->upload       = new UploadFile();
    }

    public function index(Request $request, Response $response): Response
    {
        $about = Content::where('type', 'Info Regist')->get();
        $result['status']   = true;
        $result['data']     = $about;
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function store(Request $request, Response $response): Response
    {
        $req = $request->getParsedBody();
       
        $payload = [
            'type'      => 'Info Regist',
            'attr'      => [
                'text_en' => $req['text_en'],
                'text_id' => $req['text_id'],
                'desc_en' => $req['desc_en'],
                'desc_id' => $req['desc_id'],
                'howto_regis_en' => $req['howto_regis_en'],
                'howto_regis_id' => $req['howto_regis_id'],
                'info_en' => $req['info_en'],
                'info_id' => $req['info_id'],
                'terms_en' => $req['terms_en'],
                'terms_id' => $req['terms_id'],
            ]
        ];

       
        if(Content::where('type', 'Info Regist')->first() != ''){

            $news = Content::where('type', 'Info Regist')->first();
            $news -> update($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil diubah';

        }else{      

            Content::create($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil ditambah';

        }
        return JsonResponse::withJson($response, $result, 200);
    }


    
}
<?php

namespace App\Controller;

use App\Model\News;
use App\Model\User;
use Pimple\Psr11\Container;
use App\Helper\JsonResponse;
use App\Repository\UploadFile;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class NewsController
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        // $this->upload       = new UploadFile();
    }

    public function index(Request $request, Response $response): Response
    {
        // $payload = $request->getParsedBody();
        
        $result['status']   = true;
        $news = News::all();
        // dd($news);
        $result['data']   = $news;
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function store(Request $request, Response $response): Response
    {
        $req = $request->getParsedBody();

        $payload = [
            'title_en'      => $req['title_en'],
            'title_id'      => $req['title_id'],
            'link'          => $req['link'],
            'source_name'   => $req['source_name']
        ];

        if($req["id"]){

            $news = News::find(intval($req["id"]));
            $news -> update($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil diubah';

        }else{

            News::create($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil ditambah';

        }
        return JsonResponse::withJson($response, $result, 200);
    }


    public function delete(Request $request, Response $response): Response
    {
        $params = $request->getQueryParams();
        $id     = isset($params['id']) ? $params['id'] : null;

        if($id){

            News::find($id)->delete();
            $result['status']   = true;
            $result['message']   = 'data berhasil dihapus';

        }else{

            $result['status']   = false;
            $result['message']   = 'data tidak di temukan';
            
        }
        
        return JsonResponse::withJson($response, $result, 200);
    }
   
    
}
<?php

namespace App\Controller;


use App\Model\ImportantDate;
use Pimple\Psr11\Container;
use App\Helper\JsonResponse;
use App\Repository\UploadFile;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ImportantDateController
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
        // $this->upload       = new UploadFile();
    }

    public function index(Request $request, Response $response): Response
    {
        // $payload = $request->getParsedBody();
        
        $result['status']   = true;
        $data = ImportantDate::all();
        $result['data']   = $data;
        
        return JsonResponse::withJson($response, $result, 200);
    }

    public function store(Request $request, Response $response): Response
    {
        $req = $request->getParsedBody();

        $payload = [
            'name_en'      => $req['name_en'],
            'name_id'      => $req['name_id'],
            'desc_en'      => $req['desc_en'],
            'desc_id'      => $req['desc_id'],
            'start_date'      => $req['start_date'],
            'end_date'      => $req['end_date'],
        ];

        if($req["id"]){

            $data = ImportantDate::find(intval($req["id"]));
            $data -> update($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil diubah';

        }else{

            ImportantDate::create($payload);
            $result['status']   = true;
            $result['message']   = 'data berhasil ditambah';

        }
        return JsonResponse::withJson($response, $result, 200);
    }


    public function delete(Request $request, Response $response): Response
    {
        $params = $request->getQueryParams();
        $id     = isset($params['id']) ? $params['id'] : null;

        if($id){

            ImportantDate::find($id)->delete();
            $result['status']   = true;
            $result['message']   = 'data berhasil dihapus';

        }else{

            $result['status']   = false;
            $result['message']   = 'data tidak di temukan';
            
        }
        
        return JsonResponse::withJson($response, $result, 200);
    }
   
    
}
<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Content extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'content';

	protected $guarded = [];
	protected $casts   = [
        'attr'    => 'array'
    ];

    
}

<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Jury extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'jury';

	protected $guarded = [];

}

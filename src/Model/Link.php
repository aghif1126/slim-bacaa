<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Link extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'list_link';

	protected $guarded = [];

    
}

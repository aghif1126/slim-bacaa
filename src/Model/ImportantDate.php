<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ImportantDate extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'important_date';

	protected $guarded = [];

    
}

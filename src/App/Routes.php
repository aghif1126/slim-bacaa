<?php

declare(strict_types=1);

// auth admin dan staff
$app->get('/', 'App\Controller\HomeController:index');
$app->post('/login', 'App\Controller\AuthController:login');
$app->post('/register', 'App\Controller\AuthController:register');


$app->get('/news', 'App\Controller\NewsController:index');
$app->get('/news/delete', 'App\Controller\NewsController:delete');
$app->post('/news/action', 'App\Controller\NewsController:store');

$app->get('/about-bio', 'App\Controller\AboutBioController:index');
// $app->get('/about-bio/delete', 'App\Controller\AboutBioController:delete');
$app->post('/about-bio/action', 'App\Controller\AboutBioController:store');

$app->get('/about-seleksi', 'App\Controller\AboutSeleksiController:index');
$app->post('/about-seleksi/action', 'App\Controller\AboutSeleksiController:store');

$app->get('/jury', 'App\Controller\JuryController:index');
$app->get('/jury/delete', 'App\Controller\JuryController:delete');
$app->post('/jury/action', 'App\Controller\JuryController:store');

$app->get('/award', 'App\Controller\AwardController:index');
$app->post('/award/action', 'App\Controller\AwardController:store');

$app->get('/awards', 'App\Controller\AwardsController:index');
$app->get('/awards/delete', 'App\Controller\AwardsController:delete');
$app->post('/awards/action', 'App\Controller\AwardsController:store');

$app->get('/links', 'App\Controller\LinkController:index');
$app->get('/links/delete', 'App\Controller\LinkController:delete');
$app->post('/links/action', 'App\Controller\LinkController:store');

$app->get('/important-date', 'App\Controller\ImportantDateController:index');
$app->get('/important-date/delete', 'App\Controller\ImportantDateController:delete');
$app->post('/important-date/action', 'App\Controller\ImportantDateController:store');

$app->get('/info-regist', 'App\Controller\InfoRegistController:index');
$app->post('/info-regist/action', 'App\Controller\InfoRegistController:store');

$app->get('/preference', 'App\Controller\PreferenceController:index');
$app->post('/preference/action', 'App\Controller\PreferenceController:store');
